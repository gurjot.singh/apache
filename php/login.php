<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Self Serve - Login Page</title>
        <link rel="stylesheet" href="/styles/styles.css">
        <div class="header">
            <h1>
                <img style="vertical-align:top;margin:-5px 0px" src="/images/Datex.png" alt="Datex" width=40 height=40></img><img src="/images/Bell.png" alt="Bell" width=90 height=50></img> 
                Self Serve Login Page
            </h1>
            <hr>
        </div>
        <script src="/javascript/init.js"></script> 
    </head>
    <body>
        <form id="form1"
            action=""
            method="post">
            <div id="div_alert" class="alert" style="display: none;"><span id="alert" class="closebtn" onclick="close();">&times;</span> 
                  <strong>!</strong> Please enter the credentials and accept the terms &amp; conditions.
            </div>
            <br>
            User name: <input type="text" name="name"><br><br>
            Password: <input type="password" name="password"><br><br>
            Host name:
                <select name="host">
                    <option value="">Please select</option>
                    <option value="caddwd-536.belldev.dev.bce.ca">caddwd-536.belldev.dev.bce.ca</option>
                    <option value="ipact-dbr-on.int.bell.ca">ipact-dbr-on.int.bell.ca</option>
                </select> <br><br>
            <input type="checkbox" name="tc" value="ok">
                I accept the terms &amp; conditions <br><br>
            <input type="submit" id="submit" name="submit" value="Login"><br> <br>
            <button onClick="window.print()">Print this page</button><br> <br>
        </form>
        <hr>
        <p>
            <?php
                $name = '';
                $password = '';
                $tc = '';
                $host = '';

                if (isset($_POST['submit'])) {
                    // echo htmlspecialchars($_POST['searchterm'],ENT_QUOTES);
                     if (isset($_POST['name']) and isset($_POST['password']) and isset($_POST['tc'])) {
                        $name = $_POST['name'];
                        $password = $_POST['password'];
                        $host = $_POST['host'];
                        $tc = $_POST['tc'];
                        if ($name!='' and $password!='' and $tc=='ok' and $host!='') {
                            // printf('User name: %s',htmlspecialchars($name,ENT_QUOTES));
                            printf('<p><b>Host Name:</b> %s </p><br>',$host);
                            printf('<iframe style="border: none;" src="https://kibana-datastealth.apps.ocp-dev-ade.belldev.dev.bce.ca/app/visualize#/edit/86ef8210-2f4e-11eb-870b-63703208df3c?embed=true&_g=(filters:!(),refreshInterval:(pause:!t,value:0),time:(from:now-30d,to:now))&_a=(query:(language:lucene,query:\'Header.HostName.keyword:%s\'))" height="400" width="700"></iframe>',$host);
                            printf('<iframe style="border: none;" src="https://kibana-datastealth.apps.ocp-dev-ade.belldev.dev.bce.ca/app/visualize#/edit/006f37b0-2f50-11eb-870b-63703208df3c?embed=true&_g=(filters:!(),refreshInterval:(pause:!t,value:0),time:(from:now-30d,to:now))&_a=(query:(language:lucene,query:\'Header.HostName.keyword:%s\'))" height="400" width="700"></iframe>',$host);
                            printf('<script language="javascript">show();</script>');
                        
                        };
                     }
                    //  else {
                    //     printf('<script language="javascript">unhide();</script>');
                    //  };
                }
            ?>
        </p>
    </body>
</html>
